# My Map

### Get started

- run `npm install` to setup package dependencies.
- then run `npm start`

### Resources

- [leaflet](http://leafletjs.com/)
- [express](http://expressjs.com/)
- [ejs](http://ejs.co/)
