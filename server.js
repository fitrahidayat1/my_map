#!/usr/bin/env node

// module dependencies
const express = require("express");

// get port from environment and store in Express
const app = express();

// set the view engine to ejs
app.set("view engine", "ejs");

// use res.render to load up an ejs view file
// index page
app.get("/", function (req, res) {
  res.render("pages/index");
});

// Get port from environment and store in Express
const port = process.env.PORT || 3000;
app.listen(port);
console.log("Server is listening on port", port);
